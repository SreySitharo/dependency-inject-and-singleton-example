package com.example.core_other_test;

import com.example.core_other_test.model.Car;
import com.example.core_other_test.model.Person;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class CoreOtherTestApplication implements CommandLineRunner {

    private final Car car;

    public CoreOtherTestApplication(Car car) {
        this.car = car;
    }

    public static void main(String[] args) {
        SpringApplication.run(CoreOtherTestApplication.class, args);
//        Logger logger = LoggerFactory.getLogger(CoreOtherTestApplication.class);
//
//        ApplicationContext applicationContext = SpringApplication.run(CoreOtherTestApplication.class, args);
//        logger.info("Person bean {} : " + applicationContext.getBean("person"));
//        Person personOne = applicationContext.getBean(Person.class);
//        Person personTwo = applicationContext.getBean(Person.class);
//
//        System.out.println("Person 1 :  " + personOne);
//        personOne.setFirstName("Kok");
//        System.out.println("Person 1 :  " + personOne);
//        System.out.println("Person 2 :  " + personTwo);
    }

    @Override
    public void run(String[] args) throws Exception {
        car.driving();
    }
}
