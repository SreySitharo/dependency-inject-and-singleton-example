package com.example.core_other_test.config;

import com.example.core_other_test.model.Person;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class ConfigBean {
    @Bean
    @Scope("prototype")
    public Person person() {
        return new Person("Johny", "Son");
    }
}
