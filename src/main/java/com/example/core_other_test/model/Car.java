package com.example.core_other_test.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Car {

//    Constructor Injection
//    private final Person person;
//    private final Engine engine;
//
//    public Car(Person person, Engine engine) {
//        this.person = person;
//        this.engine = engine;
//    }

//    Setter Injection
//    @Autowired
//    public void setPerson(Person person) {
//        this.person = person;
//    }
    @Autowired
    private Person person;
    @Autowired
    private Engine engine;

    public void driving() {
        engine.setEngineName("Toyota");

        System.out.println("Engine name : " + engine.getEngineName());
        System.out.println(person.getFirstName() + " is driving !!");
        System.out.println("Inside the driving method !!");
    }
}
